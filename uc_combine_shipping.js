// $Id$

/**
 * @file
 * Combine shipping quotes
 */


var uc_combined_details;
var uc_combined_line_items;

/**
 * Retrieve a list of available shipping quotes.
 *
 * @param products
 *   Ignored. The product data in Drupal.settings stored by the uc_combine_shipping module is used instead.
 */
function quoteCallback(products) {
  var updateCallback = function (progress, status, pb) {
    if (progress == 100) {
      pb.stopMonitoring();
    }
  };

  // page is always the same for each shipping method
  page = $("input:hidden[name*=page]").val();
  // use an array for the details
  uc_combined_details = new Array();
  // empty the quote div now, each shipping method will have its own sub-div
  $("#quote").empty()
  // need to add a sub-div where an input field is hidden, this is the value that Ubercart will receive 
  $("#quote").append('<div id="quote-uc-combined-shipping"></div>');
  $("#quote-uc-combined-shipping").append('<input type="radio" name="quote-option" value="uc_combine_shipping---uc_combine_shipping" style="display: none" checked="checked"/>');
  // need an array where the shipping method ids will be stored, helps setting the line items
  uc_combined_line_items = new Array();
  
  // send a request for each shipping method, display results one by one
  if (typeof(Drupal.settings.uc_combine_shipping)=='object' && typeof(Drupal.settings.uc_combine_shipping.products)=='object') {
    for (var shipping_method in Drupal.settings.uc_combine_shipping.products) {  
      uc_combined_details[shipping_method] = new Object();
  
      uc_combined_details[shipping_method]["uid"] = $("input[name*=uid]").val();
      //details["details[zone]"] = $("select[name*=delivery_zone] option:selected").val();
      //details["details[country]"] = $("select[name*=delivery_country] option:selected").val();
    
      $("select[name*=delivery_]").each(function(i) {
        uc_combined_details[shipping_method]["details[delivery][" + $(this).attr("name").split("delivery_")[1].replace(/]/, "") + "]"] = $(this).val();
      });
      $("input[name*=delivery_]").each(function(i) {
        uc_combined_details[shipping_method]["details[delivery][" + $(this).attr("name").split("delivery_")[1].replace(/]/, "") + "]"] = $(this).val();
      });
      $("select[name*=billing_]").each(function(i) {
        uc_combined_details[shipping_method]["details[billing][" + $(this).attr("name").split("billing_")[1].replace(/]/, "") + "]"] = $(this).val();
      });
      $("input[name*=billing_]").each(function(i) {
        uc_combined_details[shipping_method]["details[billing][" + $(this).attr("name").split("billing_")[1].replace(/]/, "") + "]"] = $(this).val();
      });
  
      // store product details
      uc_combined_details[shipping_method]["products"] = Drupal.settings.uc_combine_shipping.products[shipping_method];
      
      // create a sub-div and add a progress bar to it
      var shipping_method_id = "quote-" + shipping_method.replace('_', '-');
      var progress = new Drupal.progressBar("quoteProgress");
      progress.setProgress(-1, Drupal.settings.uc_quote.progress_msg);
      $("#quote").append('<div id="' + shipping_method_id + '"></div>');
      $('#'+shipping_method_id).append(progress.element);
      $('#'+shipping_method_id).addClass("solid-border");
      // progress.startMonitoring(Drupal.settings.basePath + "?q=shipping/quote", 0);
      $.ajax({
        type: "POST",
        url: Drupal.settings.ucURL.shippingQuotes,
        data: uc_combined_details[shipping_method],
        dataType: "json",
        success: displayQuote
      });
    }
  }

  return false;
}

/**
 * Parse and render the returned shipping quotes.
 */
function displayQuote(data) {
  // get the shipping method that has been stored by the uc_combine_shipping module
  var shipping_method = 'default';
  if (typeof(data['uc_combine_shipping---uc_combine_shipping'])=='object') {
    if (typeof(data['uc_combine_shipping---uc_combine_shipping']['shipping_method'])=='string') {
      shipping_method = data['uc_combine_shipping---uc_combine_shipping']['shipping_method'];
    }
  }
  var shipping_method_id = "quote-" + shipping_method.replace('_', '-');
  // move the entries of the given shipping method to data_shipping
  var data_shipping = new Object;
  for (i in data) {
    if (i.substr(0, shipping_method.length) === shipping_method) {
      data_shipping[i] = data[i];
    }
  }  
  // keep data of only this shipping method, discarding any other quotes
  data = data_shipping;

  // it is possible that this function is called more than once with the same shipping data
  // therefore we need to assign and find a number for this shipping method
  // A unique number is needed for the line items (shipping_1, etc.), this unique number
  // will be the position of the shipping method inside the Drupal.settings array.
  var shipping_method_counter = 0;
  if (typeof(Drupal.settings.uc_combine_shipping)=='object' && typeof(Drupal.settings.uc_combine_shipping.products)=='object') {
    shipping_method_counter = 1;
    for (var shipping_method_entry in Drupal.settings.uc_combine_shipping.products) {  
      if (shipping_method_entry==shipping_method) {
        // found, use current counter
        break;
      }
      shipping_method_counter++;
    }
  }
  

  var quoteDiv = $('#'+shipping_method_id).empty();
  var numQuotes = 0;
  var errorFlag = true;
  var i;
  var display_no_default = false;
  for (i in data) {
    if (data[i].rate != undefined || data[i].error || data[i].notes) {
      numQuotes++;
    }
  }
  for (i in data) {
    var item = '';
    var label = data[i].option_label;
    // do not set the default radio button if the shipping quote contains the 'display_no_default' setting
    if (typeof(data[i].display_no_default)!='undefined' && data[i].display_no_default) {
      display_no_default = true;
    }
    if (data[i].rate != undefined || data[i].error || data[i].notes) {

      if (data[i].rate != undefined) {
        // do not display the price if the shipping quote contains the 'display_no_prices' setting
        var price_format;
        if (typeof(data[i].display_no_prices)!='undefined' && data[i].display_no_prices) {
          price_format = '';
        }
        else {
          price_format = ": " + data[i].format;
        }
        if (numQuotes > 1 && page != 'cart') {
          item = "<input type=\"hidden\" name=\"rate[" + i + "]\" value=\"" + data[i].rate + "\" />"
            + "<label class=\"option\">"
            + "<input type=\"radio\" class=\"form-radio\" name=\"quote-option-"+ shipping_method_id +"\" value=\"" + i + "\" />"
            + label + price_format + "</label>";
        }
        else {
          item = "<input type=\"hidden\" name=\"quote-option-"+ shipping_method_id +"\" value=\""+ i + "\" />"
            + "<input type=\"hidden\" name=\"rate[" + i + "]\" value=\"" + data[i].rate + "\" />"
            + "<label class=\"option\">" + label + price_format + "</label>";
          if (page == "checkout") {
            if (label != "" && window.set_line_item) {
              set_line_item("shipping_"+shipping_method_counter, label, data[i].rate, 1);
            }
            // store the id of this input field in the corresponding field of the uc_combine_shipping checkout pane
            $('#edit-panes-uc-combine-shipping-uc-combine-shipping-'+ shipping_method_counter).val(i);
          }
          if (page == 'order-edit') {
            // store the id of this input field in the corresponding field of the uc_combine_shipping checkout pane
            $('#edit-panes-uc-combine-shipping-uc-combine-shipping-'+ shipping_method_counter).val(i);
          }
        }
      }
      if (data[i].error) {
        item += '<div class="quote-error">' + data[i].error + "</div>";
      }
      if (data[i].notes) {
        item += '<div class="quote-notes">' + data[i].notes + "</div>";
      }
      if (data[i].rate == undefined && item.length && label != undefined) {
          item = label + ': ' + item;
      }
      quoteDiv.append('<div class="form-item">' + item + "</div>\n");
      Drupal.attachBehaviors(quoteDiv);
      if (page == "checkout") {
        // store the line item key of the given option if it has not been stored yet.
        // this way the radio button knows which line item to set. Originally it was 'shipping', now it is 'shipping_1', etc.
        if (typeof(uc_combined_line_items[i])=='undefined') {
          uc_combined_line_items[i] = "shipping_"+shipping_method_counter;
        }
        // Choosing to use click because of IE's bloody stupid bug not to
        // trigger onChange until focus is lost. Click is better than doing
        // set_line_item() and getTax() twice, I believe.
        quoteDiv.find("input:radio[value=" + i +"]").click(function() {
          var i = $(this).val();
          if (window.set_line_item) {
            // get the key that belongs to this option
            var key = 'shipping';
            if (typeof(uc_combined_line_items[i])=='string') {
              key = uc_combined_line_items[i];
            }
            // set line item with the given key
            set_line_item(key, data[i].option_label, data[i].rate, 1, 1);
          }
          // store the id of the selected radio button in the corresponding field of the uc_combine_shipping checkout pane
          $('#edit-panes-uc-combine-shipping-uc-combine-'+ key.replace('_', '-')).val(i);
        });
      }
      if (page == "order-edit") {
        // store the line item key of the given option if it has not been stored yet.
        // this way the radio button knows which line item to set. Originally it was 'shipping', now it is 'shipping_1', etc.
        if (typeof(uc_combined_line_items[i])=='undefined') {
          uc_combined_line_items[i] = "shipping_"+shipping_method_counter;
        }
        // Choosing to use click because of IE's bloody stupid bug not to
        // trigger onChange until focus is lost. Click is better than doing
        // set_line_item() and getTax() twice, I believe.
        quoteDiv.find("input:radio[value=" + i +"]").click(function() {
          var i = $(this).val();
          // get the key that belongs to this option
          var key = 'shipping';
          if (typeof(uc_combined_line_items[i])=='string') {
            key = uc_combined_line_items[i];
          }
          // store the id of the selected radio button in the corresponding field of the uc_combine_shipping checkout pane
          $('#edit-panes-uc-combine-shipping-uc-combine-'+ key.replace('_', '-')).val(i);
        });
      }
    }
    if (data[i].debug != undefined) {
      quoteDiv.append("<pre>" + data[i].debug + "</pre>");
    }
  }
  if (quoteDiv.find("input").length == 0) {
    quoteDiv.append(Drupal.settings.uc_quote.err_msg);
  }
  else {
    
    if (!display_no_default) {
      // set the default radio button, unless desired otherwise
      quoteDiv.find("input:radio").eq(0).click().attr("checked", "checked");
    }
    var quoteForm = quoteDiv.html();
    quoteDiv.append("<input type=\"hidden\" name=\"quote-form\" value=\"" + Drupal.encodeURIComponent(quoteForm) + "\" />");
  }

  /* if (page == "checkout") {
    if (window.getTax) {
      getTax();
    }
    else if (window.render_line_items) {
      render_line_items();
    }
  } */
}


