v1.7 --- 2011/07/23

Note: This has been designed for Drupal 6.x and Ubercart 2.x

Appendix:
-------------
1.0	Purpose
	- Intended purpose / use of this module
2.0	New Install
	- This explains the steps for a fresh site with no products added to the store.
3.0	Existing Install
	- This explains the setup for an existing Ubercart site that is assumed to be a production site with shipping & payment configured and working (or) close to being ready for production.
4.0	Notes
	- Various information that needs to be mentioned, but doesn't fit within the install procedure.
5.0	Troubleshooting
6.0	Known Issues
	- Currently known issues regarding the module and project.




1.0	Purpose:
-------------
The purpose of the "Ubercart Combine Shipping" module is to provide a way of explicitly forcing the method of shipping for a product or product class.  Instead of having three shipping methods enabled on the cart and providing a choice to the customer for the best price, you will be able to specify at the product level, which shipping method a particular product will ship with.  For example, PRODUCT A will ship UPS, PRODUCT B will ship FedEx.  When the customer checks out, instead of providing an option for UPS or FedEx to the customer, both the UPS shipping price and the FedEx shipping price will be combined into one single shipping price.

The Ubercart Combine Shipping module is designed to be very flexible from the standpoint that you "should" be able to mix and match any method of shipping for your site.  While it's been tested primarily with UPS, Flat Rate and a custom Freight method, the modules design is for all possible shipping methods designed for Ubercart 2.x.




2.0	New Install:
-------------
2.1	Install and setup Ubercart to your liking.
	- The Combine Shipping module should be installed and setup before adding products.  Basically whenever you are ready to start configuring the shipping methods.

2.2	Enable any shipping methods you want to use for the site (ex. UPS, FedEx, Flat Rate, etc.) Make sure they are enabled and configured at: "/admin/store/settings/quotes/methods"

2.3	Download

2.4	Copy to "/sites/all/modules" folder (or folder of choice)

2.5	Enable at: "/admin/build/modules" (or) drush en -y uc_combine_shipping

2.6	Once enabled, a new CCK field "shipping method" will be automatically created for the product content type and any classes.  The enabled shipping methods (step 2.2) will be automatically added to the "Allowed values list".  If you enable a shipping module at a later time, you will first have to enable the new shipping method at: "/admin/store/settings/quotes/methods", then you will need to manually add that shipping method to the "Shipping Method" CCK fields "Allowed values list" with a "key|label" option format.  All methods will be referenced within "Allowed values list" help area with the proper "key|suggested label" to make life a little easier (You can just copy and paste the new value, then edit the label part to suit your fancy).  Keep in mind this will be required for both the product content type and any product class content types.

2.7	Upon enabling the module, there is no default shipping method set for the product / classes "Shipping Method" CCK field.  Edit this field and look for "Product Settings", which will let you define the default method for shipping.  Setting the default value to N/A will NOT set a default value for new products when creating a new product node and you will have to choose which shipping method to use during the product's creation.  (It's a required field and will force you to choose before saving)  Otherwise, if you have let say: UPS and FedEx enabled and choose FedEx as the default, all new products / classes will be created using that default value but you will be able to change that default value at will for each individual product.

2.8	When you are ready to create a product, you will notice a "Shipping Method" radio option listing the available options set in the CCK field.

(See the "Notes:" section below for additional information)




3.0	Existing Install:
-------------
3.1	Ubercart is assumed to already be installed, products created for the site, shipping setup, payment transactions etc.

3.2	Existing shipping methods are assumed to already be setup, be sure to enable any new shipping methods you wish to use on the site from this point forward. (ex. UPS, FedEx, Flat Rate, Freight, etc.)  Make sure they are enabled and configured at: "/admin/store/settings/quotes/methods"

3.3	Download

3.4	Copy to "/sites/all/modules" folder (or folder of choice)

3.5	Enable at: "/admin/build/modules" (or) drush en -y uc_combine_shipping

3.6	Once enabled, a new CCK field "shipping method" will be automatically created for the product content type and any existing classes.  The enabled shipping methods (step 3.2) will be automatically added to the "Allowed values list".  If you enable a shipping method at a later time, you will first have to enable the new shipping method at: "/admin/store/settings/quotes/methods", then you will need to manually add that shipping method to the "Shipping Method" CCK fields "Allowed values list" with a "key|label" option format.  All methods will be referenced within "Allowed values list" help area with the proper "key|suggested label" to make life a little easier (You can just copy and paste the new value, then edit the label part to suit your fancy).  Keep in mind this will be required for both the product content type and any product class content types.

3.7	Upon enabling the module, there is no default shipping method set for the product / classes "Shipping Method" CCK field.  Edit this field and look for "Product Settings", which will let you define the default method for shipping.  Setting the default value to N/A will NOT set a default value for new products when creating a new product node and you will have to choose which shipping method to use during the product's creation.  (It's a required field and will force you to choose before saving)  Otherwise, if you have let say: UPS and FedEx enabled and choose FedEx as the default, all new products / classes will be created using that default value but you will be able to change that default value at will for each individual product.

3.8	When you are ready to create a new product, you will notice a "Shipping Method" radio option listing the available options set in the CCK field.

3.9	For the already existing product nodes, because of the limitation with CCK not applying a default value to existing nodes, you will need to manually set the shipping method for a product if you edit, then save the product (because the field is required).  However, the Combine Shipping module is designed with existing sites in mind.  All products on the site will inherit the Default Value that was set for the Shipping Method CCK field.  This should simplify the installation by not requiring you to edit every product on the site and manually setting the shipping method.  What you WILL have to do though, is edit the "special" products that you need to ship through an alternative method and explicitly set that product.  The idea is to manually set every product over time as you get to it instead of micromanaging everything from the beginning.  If you have a handful of products, it's recommended to set the shipping method for all of the existing products.  But if you have a lot of products, just focus on the products that need the alternate shipping.  (For manually setting the existing products, see the "Notes" section below)

(See the "Notes:" section below for additional information)




4.0 Notes:
-------------
4.1	There is no Admin interface for Combine Shipping.  The only place to make any changes are within the CCK field "Shipping Method"
4.2	Information about the installed module can be found at: "/admin/reports/status" under the heading of: "Ubercart Combine Shipping" (More info can be found in the "Troubleshooting" section)
4.3	The Shipping Method CCK field should be setup properly out of the box with the only two areas of needed configuration being the "Allowed Value List" and "Default Value".  If you want to tweak the other options, feel free but there are no guarantees it will work.
4.4	Product Kits will pull the quotes from the respective products within the kit.  No shipping method CCK field is required for this content type.
4.5	If you create a NEW product class AFTER enabling "Combine Shipping", you must first disable the "Combine Shipping" module and save, then re-enable the "Combine Shipping" module (and save).  This will automatically create a "Shipping Method" CCK field for the newly created class.  From there you can maintain the defaults for the shipping method on that class.
4.6	Manually setting the Shipping Method CCK field for existing products can seem like a daunting task if there are quite a few products.  The easiest way to set this field for existing products is to create a view using views bulk operations and setting the value using a batch method instead of editing each node:
	1) Install Views: http://drupal.org/project/views (If you don't know how to use Views, you might do some reading up on it first)
	2) Install Views Bulk Operations: http://drupal.org/project/views_bulk_operations
	3) Create a new view setting the "Style" to "Bulk Operations".
		- Make sure "Selected operations:" has a check next to: "Modify node fields (views_bulk_operations_fields_action)"
		- Open "Modify node fields" and make sure the "Display fields:" is set to "Shipping Method (field_uc_shipping_method)"
	4) Add whichever fields will make the most sense to you (Ex. Image, Title, SKU, etc.)
	5) Add the typical filters: (Published, Node type = product, [ubercart class] etc.,) and also add a filter for "Content: Shipping Method (field_uc_shipping_method)" setting the "Operator:" to "Is Empty (NULL)
	6) From here you can do some minor tweaks to the view to set it up how you want, but the overall outcome will give you a view where you can put a checkbox next to all of the products you want to explicitly set the shipping on and it will apply that value in a batch process.  Having set the filter to "Is Empty (NULL)" means that when you apply the shipping method to a product, it will dissappear from your view so you won't keep updating the same products.




5.0 Troubleshooting:
-------------
5.1 Displayed errors from the reports status page "/admin/reports/status"
	Error: Content Type / Ubercart Class has no shipping method CCK field, but it should have. 
		Solution: disable and re-enable the "Combine Shipping method" module and the CCK field will be automatically created
	Error: A product has no shipping method value and the content type has no default shipping method. 
		Solution 1: There are links to the edit page of these nodes (maximum 10 links are displayed), and you can click on the links and set the shipping method one by one.
		Solution 2: set a default shipping method for the content type as described earlier.


6.0 Known Issues:
-------------
PHP Code is NOT working with the CCK Shipping Method field "default value" and the textarea box has been disabled.